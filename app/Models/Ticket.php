<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class Ticket extends BaseModel
{
    use HasFactory;

    function tariffs() {
      return $this->hasMany(Tariff::class);
    }

}
