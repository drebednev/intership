<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class Carrier extends BaseModel
{
    use HasFactory;

    protected $hidden = [
      'status_id',
      'created_at',
      'updated_at'
    ];

    function carrier_status() {
      return $this->belongsTo(CarrierStatus::class, 'status_id');
    }

}
