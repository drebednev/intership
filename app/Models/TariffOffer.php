<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class TariffOffer extends BaseModel
{
    use HasFactory;

    protected $fillable = [
      'tariff_if',
      'name',
      'description',
      'price'
    ];

    protected $hidden = [
      'tariff_id',
      'created_at',
      'updated_at'
    ];

}
