<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class Tariff extends BaseModel
{
    use HasFactory;

    protected $fillable = [
      'ticket_id',
      'name',
      'title',
      'description'
    ];

    protected $hidden = [
      'ticket_id',
      'created_at',
      'updated_at'
    ];

    function offers() {
      return $this->hasMany(TariffOffer::class);
    }

}
