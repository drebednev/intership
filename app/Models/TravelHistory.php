<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class TravelHistory extends BaseModel
{
    use HasFactory;

    protected $hidden = [
      'carrier_id',
      'updated_at'
    ];

    function carrier() {
      return $this->belongsTo(Carrier::class);
    }

}
