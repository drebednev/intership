<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class CarrierStatus extends BaseModel
{
    use HasFactory;
}
