<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\TravelHistoryResource;
use App\Models\TravelHistory;

class TravelHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      //FILTER
      $elements = new TravelHistory;
      if ($request->has('date')) {
        $elements = $elements->where('created_at', 'LIKE', "%{$request->input('date')}%");
      }
      if ($request->has('carrier_id')) {
        $elements = $elements->where('carrier_id', '=', $request->input('carrier_id'));
      }

      //SORT
      //$elements = $elements->orderBy('created_at', 'desc');

      //$elements = $elements->groupBy('created_at');

      //$elements = TravelHistoryResource::collection($elements->paginate(10));

      //return $elements->paginate(4);

      //return $sorted->paginate(10);

      //return $elements;

      // $grouped = [];
      // foreach ($elements as $item) {
      //   $date = date('d-m-Y', strtotime($item->created_at));
      //   $grouped[$date][] = $item;
      // }

      //$elements = $elements->groupBy('created_at');

      //return $elements;

      //return TravelHistoryResource::collection($grouped);
      return TravelHistoryResource::collection($elements->paginate(4));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
