<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\TravelHistoryResource;
use App\Models\TravelHistory;

use Illuminate\Support\Facades\Storage;

class HistoryController extends Controller
{
    public $name_view = 'modules.products.history.history';

    function showView() {


      $data = TravelHistoryResource::collection(TravelHistory::all());
      return view($this->name_view, ["data" => $data]);
    }

}
