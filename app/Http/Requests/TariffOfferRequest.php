<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TariffOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'tariff_id' => 'required|exists:tariffs,id',
          'name' => 'required',
          'description' => 'nullable',
          'price' => 'required|numeric|between:0,999999999.99'
        ];
    }
}
