<?php
namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AppService {

 static function saveImage(UploadedFile $image, $catalog) {
    $path = $image->store('uploads/' . $catalog, 'public'); //localhost/storage/uploads/
    $path = Str::replace('uploads/', '', $path);
    return $path;
  }

  static function deleteImage($path) {
    Storage::disk('public')->delete( 'uploads/' . $path);
  }

}
