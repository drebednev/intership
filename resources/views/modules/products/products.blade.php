@extends('layouts.app')

@section('connectivity')
  <link rel="stylesheet" href="{{ asset('css/products/products.css') }}">
@endsection

@section('title')Тарифы и продукты@endsection

@section('content')
  <div class="products">

    @include('common.title_content', ['title' => 'Тарифы и продукты'])

    <div class="items">

      @for ($i = 0; $i < 10; $i++)
        @include('modules.products.element',
        ['title' => 'Билет «Единый»',
        'desc' => 'Билет «Единый» позволяет совершать поездки на метро, монорельсе, автобусе, троллейбусе или трамвае. Одна поездка по билету равна одному проходу на любом виде транспорта. Билет действует на всей территории Москвы, включая Зону Б.'])
      @endfor

    </div>

  </div>
@endsection
