
<div class="element">
  <div class="container">
    <div class="place_date-box">
      <p>{{ $data->place_name }}</p>
      <p>{{ $data->created_at }}</p>
    </div>
    <div class="card-box">
      <p>{{ $data->carrier->name }}</p>
      <p>{{ $data->carrier->number }}</p>
    </div>
  </div>
  <div class="cost-box">
    <p>{{ $data->price }} ₽</p>
  </div>
</div>
