<div class="travel_history">
  @include('common.title_content', ['title' => 'История проездов'])

  <table id="table">

    <!-- {{ dd($data)->groupBy('created_at')->get(); }}  -->

    <!-- @foreach ($data as $item)
      <tr>
        <td>@include('modules.products.history.element', ['data' => $item])</td>
      </tr>
    @endforeach -->

  </table>

  <div class="pagination" id="pagination">
    <ul></ul>
  </div>

  <!-- <script src="{{ asset('js/products/history/pagination.js') }}"></script> -->
  <script>
    // items = [
    //
    //     {'place': 'Place1',
    //     'date': 'Date',
    //     'cardName': 'CARD name',
    //     'cardNum': 'CARD num',
    //     'cost': 'Cost'},
    //     {'place': 'Place2',
    //     'date': 'Date',
    //     'cardName': 'CARD name',
    //     'cardNum': 'CARD num',
    //     'cost': 'Cost'},
    //     {'place': 'Place3',
    //     'date': 'Date',
    //     'cardName': 'CARD name',
    //     'cardNum': 'CARD num',
    //     'cost': 'Cost'},
    //     {'place': 'Place4',
    //     'date': 'Date',
    //     'cardName': 'CARD name',
    //     'cardNum': 'CARD num',
    //     'cost': 'Cost'},
    //     {'place': 'Place5',
    //     'date': 'Date',
    //     'cardName': 'CARD name',
    //     'cardNum': 'CARD num',
    //     'cost': 'Cost'}
    //
    // ];
    // onTable = 3;
    // fillTable(1);
  </script>
</div>
