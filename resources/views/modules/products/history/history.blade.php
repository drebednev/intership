@extends('layouts.app')

@section('connectivity')
  <link rel="stylesheet" href="{{ asset('css/products/history.css') }}">
@endsection

@section('title') История @endsection

@section('content')

@include('common.back_navigation')

<div class="product_history">

  @include('modules.products.history.filter')

  @include('modules.products.history.travel_history')

</div>

@endsection
