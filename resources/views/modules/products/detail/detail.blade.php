@extends('layouts.app')

@section('connectivity')
  <link rel="stylesheet" href="{{ asset('css/products/detail.css') }}">
@endsection

@section('title') Detail @endsection

@section('content')

@include('common.back_navigation')

  <div class="product_detail">

    @include('common.title_content', ['title' => 'Билет «Единый»'])

    <div class="top-box">
      <div class="card-box">
        <img src="" alt="">
        <p>Билет «Единый» позволяет совершать поездки на метро, монорельсе, автобусе, троллейбусе или трамвае. Одна поездка по билету равна одному проходу на любом виде транспорта. Билет действует на всей территории Москвы, включая Зону Б.<br><br>Срок действия билета «Единый» на 1–2 поездки — 5 дней, включая день продажи. Срок действия билета «Единый» на 60 поездок — 45 дней со дня продажи.</p>
      </div>
      @include('modules.products.detail.title_desc',
      ['title' => 'Купить билет',
      'desc' => 'Вы можете купить билет и реализовать ее на вашей карте «Тройка»'])

      @include('modules.products.detail.buy_form')
    </div>

    <div class="info-box">
      @include('common.title_content', ['title' => 'Тарифы'])
      @include('modules.products.detail.title_desc',[
      'title' => 'С лимитом поездок',
      'desc' => 'Билет «Единый» с лимитом на 1 и 2 поездки действует 5 дней с момента продажи (включая день продажи).
      Билеты на 60 поездок действуют 45 дней с момента продажи (включая день продажи) и реализуются только на карте «Тройка».'
      ])

      <div class="price_elements">
        @include('modules.products.detail.price_element', [
        'title' => '1 поездка',
        'price' => '55'
        ])
        <hr>
        @include('modules.products.detail.price_element', [
        'title' => '2 поездки',
        'price' => '110'
        ])
        <hr>
        @include('modules.products.detail.price_element', [
        'title' => '60 поездок',
        'desc' => 'Билеты на 60 поездок реализуются только на карте «Тройка».',
        'price' => '1900'
        ])
      </div>

      @include('modules.products.detail.title_desc', [
      'title' => 'Без лимита поездок',
      'desc' => 'Билет «Единый» без лимита поездок на 1 и 3 суток действует с момента первого прохода, необходимо начать использовать не позднее 10 суток с момента продажи (включая день продажи).Билеты на 30, 90 и 365 дней реализуются только на транспортной карте «Тройка» и действуют с момента записи на карту.'
      ])
      <div class="price_elements">
        @include('modules.products.detail.price_element', [
        'title' => '1 сутки',
        'price' => '230'
        ])
        <hr>
        @include('modules.products.detail.price_element', [
        'title' => '3 суток',
        'price' => '438'
        ])
        <hr>
        @include('modules.products.detail.price_element', [
        'title' => '30 дней',
        'desc' => 'Билеты на 30 дней реализуются только на транспортной карте «Тройка» и действуют с момента записи на карту.',
        'price' => '2170'
        ])
        <hr>
        @include('modules.products.detail.price_element', [
        'title' => '90 дней',
        'desc' => 'Билеты на 90 дней реализуются только на транспортной карте «Тройка» и действуют с момента записи на карту.',
        'price' => '5430'
        ])
        <hr>
        @include('modules.products.detail.price_element', [
        'title' => '365 дней',
        'desc' => 'Билеты на 365 дней реализуются только на транспортной карте «Тройка» и действуют с момента записи на карту.',
        'price' => '19500'
        ])
      </div>

      @include('modules.products.detail.title_desc',
      ['title' => 'Другие виды билетов'])
      <div class="price_elements">
        @include('modules.products.detail.price_element', [
        'title' => 'Билет на провоз 1 места багажа в метро или монорельсе',
        'price' => '60'
        ])
        <hr>
        @include('modules.products.detail.price_element', [
        'title' => '«Единый» билет на 1 календарный месяц для проезда на метро, монорельсе, троллейбусе, автобусе, трамвае',
        'desc' => 'не более 70 поездок на метро/МЦК/монорельсе',
        'price' => '2770'
        ])
      </div>

    </div>

  </div>
@endsection
