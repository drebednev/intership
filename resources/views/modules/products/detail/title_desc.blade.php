<!-- $title, desc -->
<div class="title_desc">

  <h2>{{ $title }}</h2>

  @if(isset($desc))
    <p>{{ $desc }}</p>
  @endif

</div>
