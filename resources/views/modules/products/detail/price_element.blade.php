<!-- $title, desc, price -->
<div class="price_element">
  <div class="desc-box">
    <p class="title">{{ $title }}</p>
    @if(isset($desc))
      <p class="desc">{{ $desc }}</p>
    @endif
  </div>
  <div class="price-box">
    <h3>{{ $price . ' ₽' }}</h3>
  </div>
</div>
