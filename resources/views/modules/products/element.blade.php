<!-- $title, desc -->
<div class="product">
  <div class="card-box">
    <img src="{{ asset('storage/uploads/3e0yWWM67NKrdEUAJMmBOgO1nDrzrCfzKeDvvyV7.png') }}" alt="">
  </div>
  <div class="container">
    <div class="text-box">
      <h2>
        {{ $title }}
      </h2>
      <p>
        {{ $desc }}
      </p>
    </div>
    <div class="button-box">
      <button type="button" name="button" class="button_main">Купить</button>
    </div>
  </div>
</div>
