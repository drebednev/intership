<ul class="list_contacts">
  <li>
    <a href="#" class="link_strictly_color_main">
      <h4 class="phone">+7 495 539-54-54</h4>
    </a>
  </li>
  <li>
    <a href="#" class="operators link_strictly_color_main">
      3210&nbsp;<span class="color_tertiary">(Билайн, МТС, Мегафон, Tele2)</span>
    </a>
  </li>
  <li>
    <a href="#" class="link">
      Контакт-центр «Московский транспорт»
    </a>
  </li>
</ul>
