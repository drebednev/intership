<header>
  <div class="content" id="content">

    <div class="left-box">
      <div class="logos-box">
        <a href="#">
          <img src="{{ asset('images/logos/mos_transport.png') }}" alt="" class="logo">
        </a>
        <div class="line"></div>
        <a href="#">
          <img src="{{ asset('images/logos/mos_metro.png') }}" alt="" class="logo">
        </a>
      </div>
      <div class="title-box">
        <a href="#" class="link_strictly_color_secondary">
          <h3>Личный кабинет<br>пассажира</h3>
        </a>
      </div>
    </div>
    <div class="middle-box">
      <div class="contacts-box">
        @include('common.header.contacts')
      </div>
    </div>
    <div class="right-box">
      <div class="user-box">
        <div class="account-box">
          @include('common.header.account')
        </div>
        <div class="line"></div>
        <button class="login-button" type="button" name="button">Выйти</button>
      </div>
      <input class="burger" type="image" name="image" src="{{ asset('images/menu.png') }}" height="32">
    </div>
  </div>
</header>
