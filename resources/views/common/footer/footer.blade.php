<footer>
  <img src="{{ asset('images/footer_pattern.png') }}" alt="" class="pattern">
  <div class="content" id="content">

    <div class="container-1">
      <p class="title">
        ГУП «Московский метрополитен»<br>
        1935 — 2019 ©
      </p><br>
      <a href="">Правовая информация</a><br><br>
      <p class="text">
        Посещая сайт <a href="#">www.mosmetro.ru</a> Вы даете согласие на обработку файлов cookie, сбор которых осуществляется ГУП «Московский метрополитен» на условиях <a href="#">Политики обработки файлов cookie</a>. Метрополитен также может использовать указанные данные для их последующей обработки системами Яндекс.Метрика и др., которая осуществляется с целью функционирования сайта <a href="#">www.mosmetro.ru</a>.
      </p>
    </div>
    <div class="container-2">
      <div class="container">
        <div class="box-1">
          <div class="case-1">
            <p class="title_mini">
              MOS.RU
            </p>
            <p class="text">
              Правительство Москвы
            </p>
          </div>
          <div class="case-2">
            <p class="title_mini">
              DT.MOS.RU
            </p>
            <p class="text">
              Департамент транспорта и развития дорожно-транспортной инфраструктуры
            </p>
          </div>
        </div>
        <div class="box-2">
          <div class="case-1">
            <p class="title_mini">
              MINTRANS.RU<br>
            </p>
            <p class="text">
              Министерство Транспорта Российской Федерации
            </p>
          </div>
          <div class="case-2">
            <p class="title_mini">
              MOSGORTRANS.RU<br>
            </p>
            <p class="text">
              Государственное унитарное предприятие Мосгортранс
            </p>
          </div>
        </div>
      </div>
      <div class="box">
        <div class="case-1">
          <p class="title_mini">
            TRANSPORT.MOS.RU<br>
          </p>
          <p class="text">
            Московский Транспорт
          </p>
        </div>
        <div class="case-2">
          <a href="#">
            <img src="{{ asset('images/download_appstore.png') }}" alt="">
          </a>
          <a href="#">
            <img src="{{ asset('images/download_googleplay.png') }}" alt="">
          </a>
        </div>
      </div>
    </div>

  </div>

</footer>
