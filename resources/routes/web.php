<?php

use Illuminate\Support\Facades\Route;

Route::get('/products', [App\Http\Controllers\Web\ProductsController::class, 'showView']);
