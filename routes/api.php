<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Intervention\Image\Facades\Image as Image;

Route::prefix('v1')->namespace('App\Http\Controllers\Api\v1')->group(function(){

  Route::apiResources(
    ['tickets' => TicketController::class],
    ['as' => 'api']
  );
  Route::get('tickets/{id}/tariffs', [App\Http\Controllers\Api\v1\TicketController::class, 'tariffs']);

  Route::apiResources(
    ['carriers' => CarrierController::class],
    ['as' => 'api']
  );

  Route::apiResources(
    ['tariffs' => TariffController::class],
    ['as' => 'api']
  );

  Route::apiResources(
    ['tariff_offers' => TariffOfferController::class],
    ['as' => 'api']
  );

  Route::apiResources(
    ['history' => TravelHistoryController::class],
    ['as' => 'api']
  );

});
