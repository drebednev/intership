<?php

use Illuminate\Support\Facades\Route;

Route::get('products', [App\Http\Controllers\Products\ProductsController::class, 'showView']);

Route::get('detail', [App\Http\Controllers\Products\DetailController::class, 'showView']);

Route::get('history', [App\Http\Controllers\Products\HistoryController::class, 'showView']);
