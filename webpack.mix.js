const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.sass', 'public/css')
    .sass('resources/sass/modules/products/products.sass', 'public/css/products')
    .sass('resources/sass/modules/products/detail/detail.sass', 'public/css/products')
    .sass('resources/sass/modules/products/history/history.sass', 'public/css/products');
