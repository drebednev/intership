var table = document.querySelector("#table");
var pagination = document.querySelector("#pagination ul");

var items = [];
var onTable = 0;

function fillTable(page) {

  let start = (page - 1) * onTable;
  let end = start + onTable;

  let pageItems = items.slice(start, end);

  pageItems.forEach((element) => {
    let tr = document.createElement('tr');
    let td = document.createElement('td');
    td.innerHTML = element;
    //td.innerHTML = @include('modules.products.history.element');

    tr.appendChild(td);
    table.appendChild(tr);
  });

  console.log(1);

  createPagination(Math.ceil(items.length/onTable), page);

  // let pagesCount = $items.count / $onTable;
  // createPagination(pagesCount, $page)

}

function createPagination(total, page) {

  var liTag = '';

  var active;
  var onclick;

  var beforePage = page - 2;
  var afterPage = page + 2;

  active = page > 1 ? "active" : "disable"
  onclick = page > 1 ? " onclick=\"createPagination("+total+", " + (page - 1) + ")\"" : ""

  liTag += "<li class=\"btn prev "+active+"\""+onclick+"><img src=\"../../../images/pagination-arrow_"+active+".png\"></li>";

  //if (page > 2) {
    //liTag += "<li class=\"first numb\" onclick=\"createPagination(total, 1)\"><span>1</span></li>";

    // if (page > 3) {
    //   liTag += "<li class=\"dots\"><span>...</span></li>";
    // }
  //}

  if (page == total) {
    beforePage = beforePage - 2;
  } else if (page == total - 1) {
    beforePage = beforePage - 1;
  }

  if (page == 1) {
    afterPage = afterPage + 2;
  } else if (page == 2) {
    afterPage = afterPage + 1;
  }

  for (var plength = beforePage; plength <= afterPage; plength++) {
    if (plength > total) {
      continue;
    }

    if (plength <= 0) {
      plength = 1;
    }

    active = page == plength ? "active" : ""

    liTag += "<li class=\"numb " + active + "\" onclick=\"createPagination("+total+", " + plength + ")\">" + plength + "</li>";
  }

  //if (page < total - 1) {
    // if (page < total - 2) {
    //   liTag += "<li class=\"dots\"><span>...</span></li>";
    // }

    //liTag += "<li class=\"last numb\" onclick=\"createPagination(total, ".concat(total, ")\"><span>").concat(total, "</span></li>");
  //}

  active = page < total ? "active" : "disable"
  onclick = page < total ? " onclick=\"createPagination("+total+", " + (page + 1) + ")\"" : ""

  liTag += "<li class=\"btn next "+active+"\""+onclick+"><img src=\"../../../images/pagination-arrow_"+active+".png\" style=\"transform:rotate(180deg);\"></li>";

  pagination.innerHTML = liTag;
}
