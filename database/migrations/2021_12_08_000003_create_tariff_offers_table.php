<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTariffOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tariff_offers', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('tariff_id')->unsigned()->default(1);
            $table->foreign('tariff_id')->references('id')->on('tariffs');

            $table->string('name');
            $table->string('description');
            $table->decimal('price', 9, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariff_offers');
    }
}
